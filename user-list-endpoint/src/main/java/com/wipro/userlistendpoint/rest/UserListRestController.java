package com.wipro.userlistendpoint.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("userinfo")
public class UserListRestController {

	@Autowired
	private UserListRestClient userListRestClient;

	@GetMapping("/countAll")
	public int getCountAllUserInfo() {
		List<UserBodyInfo> userInfoList = userListRestClient.readAllUserDetails();
		
		return userInfoList.size();
	}
	
	@GetMapping("/getUniqueUserIdsCount")
	public int getUniqueUserIdsCount() {
		List<UserBodyInfo> userInfoList = userListRestClient.readAllUserDetails();
		int uniqueUserIdCountValue;
		
		if (!userInfoList.isEmpty()) {
			long uniqueUserIdCount = userInfoList.stream()
					.map(UserBodyInfo::getUserId)
					.distinct()
					.count();
			
			uniqueUserIdCountValue = Long.valueOf(uniqueUserIdCount).intValue();
		} else {
			uniqueUserIdCountValue = 0;
		}
		
		return uniqueUserIdCountValue;
	}
	
	@GetMapping("/update")
	public UserBodyInfo findAllUserInfo() {
		List<UserBodyInfo> userInfoList = userListRestClient.readAllUserDetails();
		if (!userInfoList.isEmpty()) {
			UserBodyInfo userBodyInfoModified = userInfoList.get(3); // Not checking size, assuming it is size >=4
			userBodyInfoModified.setTitle("1800Flowers");
			userBodyInfoModified.setBody("1800Flowers");
			
			return userBodyInfoModified;
		}
		
		return null;
	}
}
