package com.wipro.userlistendpoint.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class UserListRestClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserListRestClient.class);

	
    @Value("${userlistendpont.rest.url}")
	private String userListEndpointRestUrl;

	@Autowired
	private RestTemplate restTemplate;
	
	public List<UserBodyInfo> readAllUserDetails() {
		LOGGER.info("Fetching all user details...");
		List<UserBodyInfo> userBodyList = new ArrayList<UserBodyInfo>(0);
		try {
			ResponseEntity<List<UserBodyInfo>> userBodyInfoList = restTemplate.exchange(userListEndpointRestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserBodyInfo>>() {});
			if(userBodyInfoList != null && userBodyInfoList.hasBody()){
				userBodyList = userBodyInfoList.getBody();
			}
		}
		catch (RestClientException e) {
			// should use customized exception to throw back with meaningful info
			throw new RuntimeException("Failed to retrieve the list of user endpoints.", e);
		}
		
		return userBodyList;
	}
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}
