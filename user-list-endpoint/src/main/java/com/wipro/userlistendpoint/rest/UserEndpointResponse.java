package com.wipro.userlistendpoint.rest;

@Deprecated
public class UserEndpointResponse {
	private int userListCount;
	private int noOfUniqueUserIds;

	private UserBodyInfo userBodyInfoModified;

	public UserEndpointResponse(int userListCount, int noOfUniqueUserIds,
			UserBodyInfo userBodyInfoModified) {
		this.userListCount = userListCount;
		this.noOfUniqueUserIds = noOfUniqueUserIds;
		this.userBodyInfoModified = userBodyInfoModified;
	}

	public int getUserListCount() {
		return userListCount;
	}

	public int getNoOfUniqueUserIds() {
		return noOfUniqueUserIds;
	}

	public UserBodyInfo getUserBodyInfoModified() {
		return userBodyInfoModified;
	}

}
