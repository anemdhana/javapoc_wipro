package com.wipro.userlistendpoint;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class UserListEndpointApplication {

	public static void main(String[] args) {
		System.setProperty("http.proxyHost", "proxy2.wipro.com");
		System.setProperty("http.proxyPort", "8080");
		
		SpringApplication.run(UserListEndpointApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
	    return builder.build();
	}

}
