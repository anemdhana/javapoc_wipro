package com.wipro.userlistendpoint.rest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.wipro.userlistendpoint.rest.UserBodyInfo;
import com.wipro.userlistendpoint.rest.UserListRestClient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserListRestClientTest {
	
	@Autowired
    private UserListRestClient userListRestClient;
    
    @MockBean
    private RestTemplate restTemplate;

    @Value("${userlistendpont.rest.url}")
	private String userListEndpointRestUrl;

    @Test
    public void whenReadAllUserDetails_thenReturnUserInfoList() {
        // given
        UserBodyInfo userBodyInfo1 = new UserBodyInfo(1L, 10L, "title1", "body1");
        UserBodyInfo userBodyInfo2 = new UserBodyInfo(1L, 10L, "title2", "body2");
        UserBodyInfo userBodyInfo3 = new UserBodyInfo(1L, 10L, "title3", "body3");
        UserBodyInfo userBodyInfo4 = new UserBodyInfo(1L, 10L, "title4", "body4");
        
        List<UserBodyInfo> expectedUserInfoList = Arrays.asList(userBodyInfo1, userBodyInfo2, userBodyInfo3, userBodyInfo4);

        doReturn(new ResponseEntity<List<UserBodyInfo>>(expectedUserInfoList, HttpStatus.OK)).when(restTemplate).exchange(userListEndpointRestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserBodyInfo>>() {});

        // when
        List<UserBodyInfo> actualUserInfoList = userListRestClient.readAllUserDetails();

        // then
        assertThat(actualUserInfoList).isEqualTo(expectedUserInfoList);
    }
    
    @Test
    public void whenReadAllUserDetails_thenReturnEmptyUserInfoList() {
        // given
        List<UserBodyInfo> expectedUserInfoList = new ArrayList<UserBodyInfo>();

        doReturn(new ResponseEntity<List<UserBodyInfo>>(expectedUserInfoList, HttpStatus.OK)).when(restTemplate).exchange(userListEndpointRestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserBodyInfo>>() {});

        // when
        List<UserBodyInfo> actualUserInfoList = userListRestClient.readAllUserDetails();

        // then
        assertThat(actualUserInfoList).isEqualTo(expectedUserInfoList);
    }
    
    @Test(expected = RuntimeException.class)
    public void whenReadAllUserDetails_thenThrowedRestClientException() {
        doThrow(new RestClientException("REST Url read failure!!!")).when(restTemplate).exchange(userListEndpointRestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserBodyInfo>>() {});

        // when
        userListRestClient.readAllUserDetails();
    }
}