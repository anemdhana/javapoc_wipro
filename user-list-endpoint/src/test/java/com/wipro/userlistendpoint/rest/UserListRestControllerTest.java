package com.wipro.userlistendpoint.rest;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserListRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @BeforeClass
    public static void before() {
    	System.setProperty("http.proxyHost", "proxy2.wipro.com");
		System.setProperty("http.proxyPort", "8080");
    }

    @Test
    public void shouldReturnCountOfAllUsers() throws Exception {
        this.mockMvc.perform(get("/userinfo/countAll")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("100")));
    }
    
    @Test
    public void shouldReturnDistinctCountOfAllUserIds() throws Exception {
        this.mockMvc.perform(get("/userinfo/getUniqueUserIdsCount")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("10")));
    }
    
    @Test
    public void shouldRetrieveUpdatedUserBodyTitleAndBody() throws Exception {
        this.mockMvc.perform(get("/userinfo/update")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("{'title':'1800Flowers', 'body':'1800Flowers'}"));
    }
}